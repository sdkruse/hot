### HOT: Health-Oriented Transportation Model

Welcome to the repository for the R package HOT. Please also visit the package [tutorial](https://ghi-uw.gitlab.io/hot-tutorial/) page.

#### Quick Start

```r
if (!require(devtools))
  install.packages("devtools")
library("devtools")
#devtools::install_github("HOT/HOT")
devtools::install_gitlab("GHI-UW/HOT")
library(HOT)
#example("HOT")
```

Example data files may be found in HOT/inst. To find your locally installed HOT directory, call the following:

```r
getHOTdirectory()
```

#### License

The R/qtl package is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License,
version 3, as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose. See the GNU
General Public License for more details.

A copy of the GNU General Public License, version 3, is available at
https://www.r-project.org/Licenses/GPL-3