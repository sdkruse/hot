pdf: ./NAMESPACE
	R ${R_OPTS} -e 'library(roxygen2);roxygenize("~/HOT/")'
	R CMD Rd2pdf --no-preview --force ~/HOT/

redocument:
	R -e 'library("devtools"); library("roxygen2"); document()'

check:
	$(MAKE) redocument
	R CMD build .
	R CMD check --as-cran HOT_*.*.*.tar.gz

rebuild:
	$(MAKE) redocument
	cd && R -e 'library("devtools"); install_gitlab("ghi-uw/hot", force = TRUE, upgrade = "always")';

rebuildLocal:
	$(MAKE) redocument
	cd && R -e 'library("devtools"); install("~/HOT/", force = TRUE)';

rebuild4deploy:
	$(MAKE) redocument
	cd && R -e 'library("devtools"); install_github("syounkin/HOT-mirror", auth_token = "21a1ae2b5e7eebe4120799bd1955a7b6892e5de4", force = TRUE, upgrade = "never")';

HOTData:
	cd ~/HOT-Tool/R && R -e 'source("getHOTData.R"); getLondon(); getFrance()'

London:
	$(MAKE) rebuild
	cd && cd ~/CUSSH && $(MAKE) London

London.B:
	$(MAKE) rebuild
	cd && cd ~/CUSSH && $(MAKE) London.B

London.new:
	$(MAKE) rebuild
	cd && cd ~/CUSSH && R -e "rmarkdown::render('./R/London.new.Rmd', output_file = '../html/London.new.html')"
	cd && cd ~/CUSSH && rm -vf ./London.new.md

London.all:
	$(MAKE) rebuild
	cd && cd ~/CUSSH && $(MAKE) London
	cd && cd ~/CUSSH && $(MAKE) London.B

HOTApp:
	cd ~/HOT-Tool/R && R -e 'library("shiny"); runApp("HOT", launch.browser = TRUE)'

manual:
	rm -f HOT.pdf
	R CMD Rd2pdf ~/HOT/
	cp -v ~/HOT/HOT.pdf ~/box/HOT/misc/
